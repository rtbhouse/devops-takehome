# DevOps take-home task

In this repository you'll find a very simple Django application which uses PostgreSQL DB. We want you to:

 - provide a Continuous Integration process configuration for it in which at least tests should be ran
 - provide a Continuous Delivery process configuration for it - the app should be reachable on port 80 of the target machine/VM/cluster; the DB should be deployed with the app
 - provide a configuration of a simple metrics collection - we want to keep an eye on:
     - open connections to DB
 - write a few words about how to deploy everything in README.md

Use tools that you like and/or know. We want it to be deployable with a single command on a latest Ubuntu LTS VM **or** K8S cluster. If you want to have any dependencies on cloud, it can be only GCP. Any OpenSource tools are allowed.

You don't have to think of:

 - infrastructure core - if you want to use K8S, Jenkins, etc., you can assume that the appropriate tool exists and works, just prepare config to be used in it 
 - HA/SLA
 - showing/analyzing metrics, it is enough that they will be collected 
 - showing us working solution anywhere - just provide configs & short instruction of usage

Please make a fork of this repo (can be on another platform if you prefer), work on it and then share it with us.

If you'll take any shortcuts, let us know.

**Have fun :)**

## How to run the app in dev mode

### Prerequisites
 - Docker
 - Docker Compose

### Steps
 - Build Docker image: `docker-compose build`
 - Run DB migrations: `docker-compose run --rm app python /app/manage.py migrate`
 - Run tests: `docker-compose run --rm app python /app/manage.py test`
 - Run the app itself: `docker-compose up`
 - Open the browser at http://localhost:8000 to see the site

## Notes on running in prod
 - `DJANGO_SECRET_KEY` has to be __secret__
 - there is `gunicorn` installed, use it as an app WSGI server - https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/gunicorn/
