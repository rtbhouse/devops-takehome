from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode

from todo.models import Entry


class ToDoListTestCase(TestCase):
    def test_list_view(self):
        e1 = Entry.objects.create(content="First")
        e2 = Entry.objects.create(content="Second")

        response = self.client.get(reverse('entry-list'))

        self.assertEqual(response.status_code, 200)
        self.assertInHTML(e1.content, response.content.decode())
        self.assertInHTML(e2.content, response.content.decode())

    def test_empty_list_view(self):
        response = self.client.get(reverse('entry-list'))

        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<p>No ToDo's yet, add some!</p>", response.content.decode())


class ToDoAddTestCase(TestCase):
    def test_add_todo(self):
        data = {"content": "some todo"}

        response = self.client.post(
            reverse('entry-add'),
            urlencode(data),
            content_type="application/x-www-form-urlencoded",
            follow=True,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Entry.objects.count(), 1)
        self.assertEqual(Entry.objects.first().content, data['content'])

    def test_add_todo_without_content(self):
        data = {"content": ""}

        response = self.client.post(
            reverse('entry-add'),
            urlencode(data),
            content_type="application/x-www-form-urlencoded",
            follow=True,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Entry.objects.count(), 0)
        self.assertInHTML("<li>This field is required.</li>", response.content.decode())
