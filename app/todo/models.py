from django.db import models


class Entry(models.Model):
    content = models.CharField(verbose_name='Content', max_length=255, blank=False)
