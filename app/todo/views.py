from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView

from todo.models import Entry


class EntryListView(ListView):
    model = Entry


class EntryCreateView(CreateView):
    model = Entry
    fields = ('content',)
    success_url = reverse_lazy('entry-list')
